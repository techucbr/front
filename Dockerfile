# Imagen base
FROM node:slim

#Directorio de trabajo
WORKDIR /app

#Copiar archivos
ADD build/es6-bundled /app
ADD keys /app/keys
ADD package.json /app
ADD server.js /app

#Instalar dependencias
RUN npm install

#Puerto
EXPOSE 8000 8443

#Command
CMD ["npm", "start"]
