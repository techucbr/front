var banner = "\n"+
" _______          _     _     _ ______              _       \n"+
"(_______)        | |   (_)   (_|____  \\            | |      \n"+
"    _ _____  ____| |__  _     _ ____)  )_____ ____ | |  _   \n"+
"   | | ___ |/ ___)  _ \\| |   | |  __  ((____ |  _ \\| |_/ )  \n"+
"   | | ____( (___| | | | |___| | |__)  ) ___ | | | |  _ (   \n"+
"   |_|_____)\\____)_| |_|\\_____/|______/\\_____|_| |_|_| \\_)  \n";

console.log(banner);
                                                            
var express = require('express'),
  http = require('http'),
  https = require('https'),
  bodyParser = require('body-parser'),
  fs = require('fs'),
  proxy = require('express-http-proxy');

var app = express(),
  httpPort = process.env.PORT || 8000,
  httpsPort = process.env.PORT || 8443,
  privateKey  = fs.readFileSync('keys/express.key', 'utf8'),
  certificate = fs.readFileSync('keys/express.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};
var httpsServer = https.createServer(credentials, app);
var httpServer = http.createServer(app);
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
app.use(bodyParser.json({limit: "1mb"}));
app.all('*', function(req, res, next){
  if(req.secure){
    return next();
  };
  res.redirect('https://' + req.hostname + ":" + httpsPort + req.url);
});

app.use("/api", proxy("https://api:7443", {
  proxyReqPathResolver: function(req) {
    return require('url').parse("/api" + req.url).path;
  }
}));

app.use("/app", express.static(__dirname));
app.use("/src", express.static(__dirname));

app.get("/app",function(req, res) {
  res.sendFile('index.html', {root: "."});
});
app.get("/app/:page",function(req, res) {
  res.sendFile('index.html', {root: "."});
});

httpsServer.listen(httpsPort);
httpServer.listen(httpPort);

console.log('Bank front server started on (HTTP): ' + httpPort);
console.log('Bank front server started on (HTTPS): ' + httpsPort);
